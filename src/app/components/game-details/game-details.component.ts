import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { GamesService } from '../../services/games/games.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {

  @Input() game: Array<any>;
  @Input() users: Array<any>

  loggedInUser: any;
  id: any;
  addedGame: boolean;
  gameId: any

  constructor(private usersService: UsersService,
    private activatedRoute: ActivatedRoute, 
    private authService:AuthService,
    private gamesService:GamesService) { }

  ngOnInit() {
    
    this.getGameId()
    
    //get logged in user
    this.loggedInUser = this.authService.getUser()
    const gameId = this.gameId

    //check if logged in user have friend notifications
    if(this.loggedInUser){
      this.addedGame = !!(this.loggedInUser.myGames.find((gameFav) => {
        if (gameFav == gameId){
          return true;
        } else {
          return false;
        }
      }));
    }
  }

  getGameId() {
    this.activatedRoute.params
    .subscribe(params => {
      this.gameId = params['id'];
    })
  }

  goBack () {
    window.history.back();
  }

  handleAddClick(){
    this.activatedRoute.params
      .subscribe(params => {
        this.id = params['id'];
        this.usersService.addGameFav(this.id)
         .then((result) => {
          this.loggedInUser = result;
          this.addedGame = true;
          this.gamesService.getGame(this.id)
          .then((game) => {
            this.game = game.body[0]});
          });
      })
  };

  handleOpenTab(event, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tabcontent = document.getElementsByClassName("initial");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    // event.currentTarget.className += " active";
  };

}
