import { Component, OnInit, Input } from '@angular/core';
import { GamesService } from '../../services/games/games.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  
  
  @Input() games: Array<any>;

  search: String

  constructor(private gamesService: GamesService) { }

  ngOnInit() {
  }

  handleSearchClick(){
    const data = {
      search: this.search
    };
    this.gamesService.search(data)
      .then((games) => {
      this.games = games.body});
  }

  goBack () {
    window.history.back();
  }

}
