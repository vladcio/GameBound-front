import { Component, OnInit, Input } from '@angular/core';
import { GamesService } from '../../services/games/games.service';

@Component({
  selector: 'app-favorite-games',
  templateUrl: './favorite-games.component.html',
  styleUrls: ['./favorite-games.component.css']
})
export class FavoriteGamesComponent implements OnInit {
  
  @Input() user: any;
  games: any;
  id: any
  

  constructor(private gamesService: GamesService) { }

  ngOnInit() {

    const data = {
      myGames: this.user.myGames
    };
    
    this.gamesService.getUserGames(data)
    .then((games) => {
    this.games = games.body});
  }

}
