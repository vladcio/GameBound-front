import { Component, OnInit, Input } from '@angular/core';
import { GamesService } from '../../services/games/games.service';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  goBack () {
    window.history.back();
  }
  
  @Input() users: Array<any>;
  
  search: String
  
  constructor(private usersService:UsersService) { }

  ngOnInit() {
 
  }

  handleSearchClick(){
    const data = {
      search: this.search
    };
    this.usersService.search(data)
      .then((users) => {
      this.users = users});
  }

}
