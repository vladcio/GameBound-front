import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private usersService: UsersService) { }

  @Input() user: any;
  @Input() loggedInUser: any;
  content: string;
  id: any

  ngOnInit() {
  }

  handleReviewClick(){
   
    this.activatedRoute.params
      .subscribe(params => {
        const content = {
          content: this.content,
          createdAt: new Date()
        };
        this.id = params['id'];
        this.usersService.addReview(this.id, content)
            .then((result) => {
              // ... handle result, reset form, etc...
              // this.router.navigate(['/users/'+ this.id])
              this.user = result;
              // window.location.reload()
              // ... maybe turn this to false if your're staying on the page - this.processing = false;
            })
      })

  };

}
