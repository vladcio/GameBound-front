import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users/users.service';
import { AuthService } from '../../services/auth/auth.service';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Component({
  selector: 'app-users-det',
  templateUrl: './users-det.component.html',
  styleUrls: ['./users-det.component.css']
})
export class UsersDetComponent implements OnInit {

  @Input() user: any;

  loggedInUser: any;
  id: any;
  content: string;
  createdAt: Date;
  rate: any;
  rateAvg: any;
  isFriend: boolean;
  isRated: boolean;
  rateUsersId: Array <any>;
  isFriendNotificated: boolean;
  isSendFriendNotificated: boolean;
  isAlreadyRequest: boolean;
  fileName:String;
  link: String;
  games: any;
  
  constructor(private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private usersService: UsersService,
    private authService: AuthService) { }

  ngOnInit() {

    //establish picture link after upload
    this.link = API_URL + this.user.picPath

    //establish logged in user
    this.loggedInUser = this.authService.getUser()

    //check if logged in user it's already friend with accesed user
    if(this.loggedInUser){
      this.isFriend = !!(this.loggedInUser.myFriends.find((friend) => {
        if (this.user._id === friend){
          return true;
        } else {
          return false;
        }
      }));
    }

    //check if logged in user have friend notifications
    if(this.loggedInUser){
      this.isFriendNotificated = !!(this.loggedInUser.friendNotification.find((friend) => {
        if (friend){
          return true;
        } else {
          return false;
        }
      }));
    }

    if(this.loggedInUser){
      this.isAlreadyRequest = !!(this.loggedInUser.friendNotification.find((friend) => {
        if (this.user._id === friend){
          return true;
        } else {
          return false;
        }
      }));
    }

    //check if logged in user already send friend notification to accesed user
    if(this.loggedInUser){
      this.isSendFriendNotificated = !!(this.loggedInUser.sendFriendNotification.find((friend) => {
        if (this.user._id === friend){
          return true;
        } else {
          return false;
        }
      }));
    }

    //get the id for rated user in order to establish the avg rate
    this.rateUsersId = []
    for (var i = 0; i < this.user.rateUser.length; i++){
      this.rateUsersId.push(this.user.rateUser[i].owner)
    }

    //check if the current accesed profile it's rated
    if(this.loggedInUser){
      this.isRated = !!(this.rateUsersId.find((friend) => {
        if (this.loggedInUser._id === friend){
          return true;
        } else {
          return false;
        }
      }));
    }

    //fix the rating to 2 decimals
    this.rateAvg = this.usersService.getUserRate(this.user).toFixed(2)
  }

  //redirect to login page
  redirect() {
    this.router.navigate(['./login']);
  }

  //go back to last visited page
  goBack () {
    window.history.back();
  }

  //send friend request to accesed user
  handleAddClick(){
    this.activatedRoute.params
      .subscribe(params => {
        this.id = params['id'];
        this.usersService.addFav(this.id)
        .then(() => {
          this.usersService.getUser(this.id)
            .then((res) => {
              this.user = res
              this.isSendFriendNotificated = true
              window.location.reload()
            });
          });
      })
  };

  //accept friend request from user who requested
  handleAcceptClick(notificationId){
    this.id = notificationId
    this.usersService.acceptFav(this.id)
     .then((result) => {
      this.user = result;
      this.isFriendNotificated = false
      this.isFriend = true
      window.location.reload()
      });
  };

  handleIgnoreClick(notificationId){
    this.id = notificationId
    this.usersService.rejectReq(this.id)
     .then((result) => {
      this.user = result;
      this.isFriendNotificated = false
      this.isFriend = false
      });
  };

  //add new photo to profile
  handleAddPhotoClick(){
    let inputEl: any = document.querySelector('#file-input');
    if(inputEl.files && inputEl.files[0]){
      const formData = new FormData();
      formData.append('uploadedFile', inputEl.files[0]);    
      this.usersService.addPhoto(this.id, formData)
      window.location.reload()
    }
  }

  //rate visited user
  handleRateClick(){
    this.activatedRoute.params
      .subscribe(params => {
        const rate = {
          rate: this.rate,
          createdAt: new Date(),
          rateAvg: this.rateAvg
        };
        this.id = params['id'];
        this.usersService.addRate(this.id, rate)
        .then(() => {
          this.usersService.getUser(this.id)
            .then((res) => {
              this.user = res
              this.rateAvg = this.usersService.getUserRate(this.user).toFixed(2)
              this.isRated = true;
            })
            this.usersService.getUser(this.id)
              .then((res) => {
                this.user = res
            });
          });
      })
  };

  handleOpenTab(event, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tabcontent = document.getElementsByClassName("initial");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    // event.currentTarget.className += " active";
  };
}
