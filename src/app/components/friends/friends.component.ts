import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  @Input() user: any;

  constructor() { }

  ngOnInit() {
  }

}
