import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  @Input() user: any;
  @Input() loggedInUser: any;
  content: string;
  messageContent: string;
  id: any;
  messageId: any;

  constructor(private activatedRoute: ActivatedRoute, private usersService: UsersService) { }

  ngOnInit() {
  }

  handleMessageClick(){
    this.activatedRoute.params
      .subscribe(params => {
        const messageContent = {
          messageContent: this.messageContent,
          createdAt: new Date()
        };
        this.id = params['id'];
        this.usersService.sendMessage(this.id, messageContent)
            .then(() => {
              // ... handle result, reset form, etc...
              // this.router.navigate(['/users/'+ this.id])
              this.usersService.getUser(this.id)
              .then((res) => {
                this.user = res
              });
              // ... maybe turn this to false if your're staying on the page - this.processing = false;
            })
      })
  };

  handleReplyClick(messageId){
    this.activatedRoute.params
      .subscribe(params => {
        const content = {
          content: this.content,
          createdAt: new Date(),
          messageId: messageId
        };
        this.id = params['id'];
        this.usersService.sendReply(this.id, content)
            .then(() => {
              this.usersService.getUser(this.id)
              .then((res) => {
                this.user = res
              });
              // ... handle result, reset form, etc...
              // this.router.navigate(['/users/'+ this.id])


              // ... maybe turn this to false if your're staying on the page - this.processing = false;
            })
      })
  };
}
