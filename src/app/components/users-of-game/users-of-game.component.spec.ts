import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersOfGameComponent } from './users-of-game.component';

describe('UsersOfGameComponent', () => {
  let component: UsersOfGameComponent;
  let fixture: ComponentFixture<UsersOfGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersOfGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersOfGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
