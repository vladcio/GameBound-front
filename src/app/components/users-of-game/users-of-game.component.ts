import { Component, OnInit, Input } from '@angular/core';
import { GamesService } from '../../services/games/games.service';

@Component({
  selector: 'app-users-of-game',
  templateUrl: './users-of-game.component.html',
  styleUrls: ['./users-of-game.component.css']
})
export class UsersOfGameComponent implements OnInit {

  @Input() user: any;
  @Input() gameId: any;

  games: any;

  constructor(private gamesService: GamesService) { }

  ngOnInit() {
    const data = {
      myGames: this.user.myGames
    };
    
    this.gamesService.getUserGames(data)
    .then((games) => {
    this.games = games.body});
  }

}
