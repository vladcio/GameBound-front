import { Component } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  loading = true;
  anon: boolean;
  user: any;
  feedbackEnabled = false;
  error = null;
  processing = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {

    //check use
    this.authService.userChange$.subscribe((user) => {
      this.loading = false;
      this.user = user;
      this.anon = !user;
    });
    this.authService.me()
  }

  //logout
  submitLogout(form) {
    this.error = '';
    this.feedbackEnabled = true;
     if(form.valid) {
      this.processing = true;
        this.authService.logout()
          .then((result) => {
          this.router.navigate(['/login']);
          })
          .catch((err) => {
            this.error = err.error.error; // :-)
            this.processing = false;
            this.feedbackEnabled = false;
          });
    }
  }
}