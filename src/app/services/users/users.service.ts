import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class UsersService {


  constructor(private http: HttpClient) { }

  // get user rate
  getUserRate(user: any): Number {
    if (!user.rateUser.length) {
      return 0;
    }
    const userRate = user.rateUser.reduce((a, b) => {
      return a + b.rate;
    }, 0) / user.rateUser.length;
      return userRate;
  }

  // get all users
  getUsers(): any {
    return this.http.get(`${API_URL}/users`)
      .toPromise()
  }

  // filter the users
  search(data): any {
    return this.http.post(`${API_URL}/users/search`, data)
      .toPromise()
  }

  // get particular user
  getUser(id): Promise<any> {
    return this.http.get(`${API_URL}/users/${id}`)
    .toPromise()
  }

  // send add friend request
  addFav(id): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/fav/${id}`,{}, options)
    .toPromise()
  }

  // add game to favorites
  addGameFav(id): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/games/fav/${id}`,{}, options)
    .toPromise()
  }

  // add profile picture
  addPhoto(id, data): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/upload/`, data, options)
    .toPromise()
  }

  // accept friend request
  acceptFav(id): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/accept/${id}`,{}, options)
    .toPromise()
  }

  rejectReq(id): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/reject/${id}`,{}, options)
    .toPromise()
  }

  // review user
  addReview(id, data): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/review/${id}`, data, options)
    .toPromise()
  }

  // send message to user
  sendMessage(id, data): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/messages/${id}`, data, options)
    .toPromise()
  }

  sendReply(id, data): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/reply/${id}`, data, options)
    .toPromise()
  }

  // rate user
  addRate(id, data): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.http.post(`${API_URL}/rate/${id}`, data, options)
    .toPromise()
  }
}
