import { TestBed, inject } from '@angular/core/testing';

import { InitAuthGuardServiceService } from './init-auth-guard-service.service';

describe('InitAuthGuardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InitAuthGuardServiceService]
    });
  });

  it('should be created', inject([InitAuthGuardServiceService], (service: InitAuthGuardServiceService) => {
    expect(service).toBeTruthy();
  }));
});
