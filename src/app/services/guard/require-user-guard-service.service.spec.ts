import { TestBed, inject } from '@angular/core/testing';

import { RequireUserGuardServiceService } from './require-user-guard-service.service';

describe('RequireUserGuardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequireUserGuardServiceService]
    });
  });

  it('should be created', inject([RequireUserGuardServiceService], (service: RequireUserGuardServiceService) => {
    expect(service).toBeTruthy();
  }));
});
