import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class NewsService {

  constructor(private http: HttpClient) { }

  // get all stories
  getList(): any {
    return this.http.get(`${API_URL}/news`)
      .toPromise()
  }

  // get particular story
  getNew(id): Promise<any> {
    return this.http.get(`${API_URL}/news/${id}`)
    .toPromise()

  }

}
