import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
// import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl + '/auth';

@Injectable()
export class AuthService {

  private user: any;
  private userChange: Subject<any> = new Subject();

  userChange$: Observable<any> = this.userChange.asObservable();

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) { }

  // set the user
  private setUser(user?: any) {
    this.user = user;
    this.userChange.next(user);
    return user;
  }

  // check user
  me(): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.get(`${API_URL}/me`, options)
      .toPromise()
      .then((user) => this.setUser(user))
      .catch((err) => {
        if (err.status === 404) {
          this.setUser();
        }
      });
  }

  // login local
  login(user: any): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.post(`${API_URL}/login`, user, options)
      .toPromise()
      .then((data) => this.setUser(data));
  }

  // signup local
  signup(user: any): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.post(`${API_URL}/signup`, user, options)
      .toPromise()
      .then((data) => this.setUser(data));
  }

  // logout 
  logout(): Promise<any> {
    const options = {
      withCredentials: true
    };
    return this.httpClient.post(`${API_URL}/logout`, {}, options)
      .toPromise()
      .then(() => this.setUser());
  }

  // facebook login
  facebookLogin() {
    window.location.href = `${API_URL}/facebook` ;
  }

  //get user
  getUser(): any {
    return this.user;
  }

}
