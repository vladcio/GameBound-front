import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class GamesService {

  constructor(private http: HttpClient) { }

  // get all the games
  getList(): any {
    return this.http.get(`${API_URL}/games`)
      .toPromise()
  }

  // get particular game
  getGame(id): Promise<any> {
    return this.http.get(`${API_URL}/games/${id}`)
    .toPromise()
  }

  // get user faorite games list
  getUserGames(data): Promise<any> {
    return this.http.post(`${API_URL}/userGames/`, data)
    .toPromise()
  }

  // filter games
  search(data): any {
    return this.http.post(`${API_URL}/games/search`, data)
      .toPromise()
  }
}
