import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../services/games/games.service';

@Component({
  selector: 'app-games-page',
  templateUrl: './games-page.component.html',
  styleUrls: ['./games-page.component.css']
})
export class GamesPageComponent implements OnInit {

  games: Array<any>

  constructor(private gamesService: GamesService) { }

  ngOnInit() {
    this.gamesService.getList()
      .then((games) => {
      this.games = games.body});
  }
}
