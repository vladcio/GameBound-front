import { Component, OnInit, Input } from '@angular/core';
import { GamesService } from '../../services/games/games.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'app-game-det',
  templateUrl: './game-det.component.html',
  styleUrls: ['./game-det.component.css']
})
export class GameDetComponent implements OnInit {
  
  game: any;
  users: any;
  
  id: String;
  
  constructor(private gameService: GamesService, private activatedRoute: ActivatedRoute, private usersService: UsersService) { }

  ngOnInit() {

    this.activatedRoute.params
    .subscribe(params => {
      this.id = params['id'];
      this.gameService.getGame(this.id)
        .then((game) => {
          this.game = game.body[0]});
    })

    this.usersService.getUsers()
      .then((users) => {
        this.users = users
      })
 

  }

}
