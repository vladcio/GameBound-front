import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  feedbackEnabled = false;
  error = null;
  processing = false;

  username: String;
  password: String;
  name: String;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.me()
  }
  submitForm(form) {
    this.error = '';
    this.feedbackEnabled = true;
    const data = {
      username: this.username,
      password: this.password,
      name: this.name
    };
    if (form.valid) {
      this.processing = true;
      this.authService.signup(data)
        .then((result) => {
          // ... handle result, reset form, etc...
          this.router.navigate(['/'])
          // ... maybe turn this to false if your're staying on the page - this.processing = false;
        })
        .catch((err) => {
          this.error = err.error.error; // :-)
          this.processing = false;
          this.feedbackEnabled = false;
        });
    }
  }
}
