import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { NewsService } from '../../services/news/news.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

news: Array<any>

constructor(private authService: AuthService, private router: Router, private newsService: NewsService) { }

  ngOnInit() {
    this.authService.me()

    this.newsService.getList()
      .then((news) => {
      this.news = news.body});
  }

  logout() {
    this.authService.logout()
      .then(() => this.router.navigate(['/login']));
  }

}
