import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news/news.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-det-page',
  templateUrl: './news-det-page.component.html',
  styleUrls: ['./news-det-page.component.css']
})
export class NewsDetPageComponent implements OnInit {

  news: Array<any>;
  newsDet: any;
  
  // id: String;

  constructor(private newsService: NewsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.newsService.getList()
    .then((news) => {
    this.news = news.body});

    // this.activatedRoute.params
    // .subscribe(params => {
    //   this.id = params['id'];
    //   this.newsService.getNew(this.id)
    //     .then((newsDet) => {
    //       this.newsDet = newsDet.body[0]});
    // })
 

  }

}
