import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetPageComponent } from './news-det-page.component';

describe('NewsDetPageComponent', () => {
  let component: NewsDetPageComponent;
  let fixture: ComponentFixture<NewsDetPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsDetPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
