import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { ActivatedRoute } from '@angular/router';
import { Session } from 'protractor';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-users-det-page',
  templateUrl: './users-det-page.component.html',
  styleUrls: ['./users-det-page.component.css']
})
export class UsersDetPageComponent implements OnInit {

  id: string;
  
  user: any;

  loggedInUser: any;

  constructor(private usersService: UsersService, private activatedRoute: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    
    this.activatedRoute.params
    .subscribe(params => {
      this.id = params['id'];
      this.usersService.getUser(this.id)
        .then((res) => {
          this.user = res
        });
    })

    this.loggedInUser = this.authService.getUser()
     
  }

}
