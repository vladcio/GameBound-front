import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDetPageComponent } from './users-det-page.component';

describe('UsersDetPageComponent', () => {
  let component: UsersDetPageComponent;
  let fixture: ComponentFixture<UsersDetPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersDetPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
