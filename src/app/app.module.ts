import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthService } from './services/auth/auth.service';
import { GamesService } from './services/games/games.service';
import { InitAuthGuardService } from './services/guard/init-auth-guard-service.service';
import { NewsService } from './services/news/news.service';
import { RequireUserGuardService } from './services/guard/require-user-guard-service.service';
import { UsersService } from './services/users/users.service';

import { AppComponent } from './app.component';
import { FavoriteGamesComponent } from './components/favorite-games/favorite-games.component';
import { GameDetComponent } from './pages/game-det/game-det.component';
import { GamesPageComponent } from './pages/games-page/games-page.component';
import { GamesComponent } from './components/games/games.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { LoginComponent } from './pages/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { NewsDetPageComponent } from './pages/news-det-page/news-det-page.component';
import { NewsDetailsComponent } from './components/news-details/news-details.component';
import { RequireAnonGuardService } from './services/guard/require-anon-guard.service';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { UsersComponent } from './components/users/users.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UsersDetPageComponent } from './pages/users-det-page/users-det-page.component';
import { UsersDetComponent } from './components/users-det/users-det.component';
import { MessagesComponent } from './components/messages/messages.component';
import { FriendsComponent } from './components/friends/friends.component';
import { UsersOfGameComponent } from './components/users-of-game/users-of-game.component';

//routes

const routes: Routes = [
  { path: '', component: HomepageComponent, canActivate: [InitAuthGuardService]},
  { path: 'games', component: GamesPageComponent, canActivate: [InitAuthGuardService]},
  { path: 'games/:id', component: GameDetComponent, canActivate: [InitAuthGuardService]},
  { path: 'login', component: LoginComponent, canActivate: [RequireAnonGuardService]},
  { path: 'news', component: NewsDetPageComponent, canActivate: [InitAuthGuardService]},
  { path: 'signup', component: SignUpComponent, canActivate: [RequireAnonGuardService]},
  { path: 'users', component: UsersPageComponent, canActivate: [InitAuthGuardService]},
  { path: 'users/:id', component: UsersDetPageComponent, canActivate: [InitAuthGuardService]},
  { path: '**', redirectTo: '/', canActivate: [InitAuthGuardService]},
];

@NgModule({
  declarations: [
    AppComponent,
    FavoriteGamesComponent,
    GameDetComponent,
    GamesPageComponent,
    GamesComponent,
    GameDetailsComponent,
    HomepageComponent,
    LoginComponent,
    MessagesComponent,
    NewsComponent,
    NewsDetPageComponent,
    NewsDetailsComponent,
    ReviewsComponent,
    SignUpComponent,
    UsersComponent,
    UsersPageComponent,
    UsersDetPageComponent,
    UsersDetComponent,
    FriendsComponent,
    UsersOfGameComponent   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [AuthService, GamesService, InitAuthGuardService, NewsService, RequireAnonGuardService, RequireUserGuardService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
